#include "datacontainer.h"
#include <QDebug>

DataContainer::DataContainer()
{
    this->dataloaded = 0;
    data_count = 0;

}


DataContainer::DataContainer(QString f)
{
    filename = f;
    path = ".";
    DataContainer();
}

DataContainer::DataContainer(QString f, QString p)
{
    filename = f;
    path = p;
    //DataContainer();
    dataloaded = 0;
    format = -1;
    xaxis = 0;
    useQVector = false;
    data_count = 0;

}

bool DataContainer::isLoaded()
{
    return (bool) dataloaded;
}

bool DataContainer::setLoaded(bool stat)
{
    dataloaded = stat;
    qDebug()<< filename <<" loaded status "<<dataloaded << endl;
    return (bool) dataloaded;
}

QVector<double> *DataContainer::rowNumbers()
{
    if (_rowNumbers.isEmpty()){
        _rowNumbers.resize(data_count);
        for (int i=0;i<data_count;i++)
            _rowNumbers[i] = i;
    }
    return &_rowNumbers;
}
