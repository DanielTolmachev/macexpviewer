#ifndef FILES_FINDER_H
#define FILES_FINDER_H
#include <qeventloop.h>
#include <qdir.h>
#include <QTimerEvent>
#include <qstring.h>
#include <qthread.h>
#include "datacontainer.h"
#include "mainwindow.h"
#include <QFileSystemWatcher>

enum LOOP
{
    noLoop,
    LoopFolder
};



//class files_finder_thrd: public QObject
//{
//    Q_OBJECT
//    QThread workerThread;
//public:
//    files_finder_thrd(MainWindow*, QString &s);
//};

class files_finder:public QObject
{
    Q_OBJECT
    MainWindow * wnd;
    void scanSingleDir(QList <DataContainer*>*,QDir *);
    int mode;    
    QFileSystemWatcher * fswatch;
    void setUpWatcher();
    void setUpWatcher(QString);
    void setUpWatcher(QStringList);
    void prepareForScan();
    void loadParams();
    QSettings *sets;
protected:
    void timerEvent(QTimerEvent *event);

public:
    QString currentFile;
    QString currentFilePath;
    int currentFileIdx;
    int loop;
    bool ScanSiblings,ScanSubdirs;
    QString path;
    QDir dir;
    files_finder();    
    files_finder(QString&);
    files_finder(MainWindow*, QString &s);
    void setDirectory(QString &s);
    void setMode(int _DirectoryScanMode)
    {
        mode = _DirectoryScanMode;
    }
    virtual ~files_finder() {}
    scan_directory();
    runloop();


    QString getNextFile(int);
    QString getPrevFile(int);

    QList <DataContainer*> *list;
    void start();
    int exit();
    void setLoop(int value);

public slots:
    void run();
    QString nextFile();
    QString prevFile();
    void setScanSiblings(bool);
    void directoryChanged(QString);
    void fileChanged(QString);
    void setFileList(QStringList);
    void rescan();
signals:
    void sigNewFileName(QString,int);
    void sigNewFile(DataContainer*);
    void sigScanComplete();
};

#endif // FILES_FINDER_H
