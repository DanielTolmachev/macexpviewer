#include "parser.h"
#include <QDebug>
#include <QTextStream>
//QTextStream cout(stdout);

Parser::Parser()
{
    newFile = 0;
    connect(this,SIGNAL(sigParseDelayed()),this,SLOT(parseDelayed()),Qt::QueuedConnection);
}

Parser::setDataContainer(DataContainer *_dc)
{
    dc = _dc;
    return 0;
}

void Parser::stop()
{
    stopped = true;
}

bool Parser::isRunning()
{
    return !stopped;
}

Parser::parseDC(DataContainer * dc)
{
    setDataContainer(dc);
    newFile++;
    //return parse();
    emit sigParseDelayed();
}

Parser::parseDelayed()
{
    if (newFile>1)
    {
        newFile--;
        return -2;
    }
    newFile = 0;
    return parse();
}

Parser::parse()
{
    stopped = false;
    int res = -1;
    bool locked=false;
    try
    {        
        if (dc->isLoaded())
        {
            qDebug() << dc->filename << " is already loaded successfully"<< endl; // << dc->data_count;
            emit sigParsingFinished(dc);
            //emit testSignal();
            res = 0;
        }
        else
        {
            locked = dc->mutex.tryLock();
            if(locked)
            {
                int res = pMEXP.loadToDataContainer(dc,&stopped);
                if(res) res = pTXT.loadToDataContainer(dc,&stopped);
                if (dc->isLoaded())
                {
                    cout<< dc->filename << " was loaded successfully"<< endl; // << dc->data_count;
                    emit sigParsingFinished(dc);
                    //emit testSignal();
                }
                else
                {
                    cout << dc->filename << " has unknown format"<< endl; // << dc->data_count;
                }

            }
            else
            {
                cout << "parser: cannot lock access to DataContainer"<<endl;
                res = -2;
            }
        }

    }
    catch(...)
    {
        cout<< "Parser::parse exception";        
    }
    stopped = true;
    if (locked) dc->mutex.unlock();
    return res;
}

