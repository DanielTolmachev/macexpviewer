#ifndef SETTINGS_H
#define SETTINGS_H
#include <QSettings>

template <typename T> T loadVariableFromQSettings(QSettings &sets, const char* varname,T default_value)
{
    QVariant value = sets.value(varname);

    if (!value.isNull())
        return value.value <T> ();
    else{
        sets.setValue(varname,default_value);
        return default_value;
    }
}


#endif // SETTINGS_H
