This program is simple viewer for ASCII files containing tab separated or comma separated values.

It can plot, it can print, it allows to quickly view files in a way image viewers do: just move a mouse wheel to quickly see all files in current folder (or in several one level folders)

This application is written in C++, using Qt framework and QWT components.

*it is called MacExpViewer, because it also support files recorded with MacExp application, used on Experimental Physics III chair TU Dortmund*