#ifndef PARSERMACEXP_H
#define PARSERMACEXP_H
//#include <c++/string>
#include <QtCore/qstring.h>
#include "datacontainer.h"
#include <QObject>
#include <QFile>
//#include "cout.h"



class parserMacExp:public QObject
{
    Q_OBJECT
    parseMacExpFile(QFile*,DataContainer*);
    void setStats(DataContainer*dc);
    bool*stopped;
    double x0,dx;
public:
    parserMacExp();
    DataContainer * load(QString &);
    loadToDataContainer(DataContainer*,bool*);
};

#endif // PARSERMACEXP_H
