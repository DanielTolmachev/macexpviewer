#ifndef PARSER_H
#define PARSER_H
#include "datacontainer.h"
#include "parsermacexp.h"
#include "parsergenerictxt.h"
#include "cout.h"


#include <QObject>

class Parser: public QObject
{
    Q_OBJECT
    parserMacExp pMEXP;
    parserGenericTxt pTXT;
    DataContainer * dc;
    bool stopped;
    int newFile;
signals:
    void sigParsingFinished(DataContainer*);
    void sigParseDelayed();
    void testSignal();
public:
    Parser();
    setDataContainer(DataContainer*);
    void stop();
    bool isRunning();
public slots:
    parseDC(DataContainer*);
    parseDelayed();
    parse();
};

#endif // PARSER_H
