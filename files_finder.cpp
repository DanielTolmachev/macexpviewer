#include "files_finder.h"
#include "settings.h"
#include <qdebug.h>
#include <QMetaObject>
#include <typeinfo>
#include <iostream>

namespace DirectoryScanMode {
int SingleDirectory = 0;
int SiblingsDirectory = 0x1;
int Subdirectories = 0x2;
}

files_finder::files_finder()
{
    //this->startTimer(0,Qt::VeryCoarseTimer);
    sets = new QSettings(COMPANYNAME,PROGRAMNAME);
    loadParams();

//    loop = LoopFolder;
//    ScanSiblings = ScanSubdirs = true ;

}

files_finder::files_finder(QString &s)
{

    setDirectory(s);
    files_finder();

}

files_finder::files_finder(MainWindow* wind, QString &s)
{
    path = s;
    wnd = wind;
    connect(wnd,MainWindow::sigAdvanceNext,this,files_finder::nextFile);
    connect(wnd,MainWindow::sigAdvancePrev,this,files_finder::prevFile);
    files_finder();

}

void files_finder::setDirectory(QString &s)
{
    path = s;
    QFileInfo fi(path);
    qDebug() << "path is "<< path;
    if (fi.isDir())  dir = QDir(fi.absoluteFilePath());
    else dir = fi.dir();
    qDebug() << "dir is "<<dir;
    path = dir.absolutePath();
    if(fi.exists())
    {
        currentFile = fi.fileName();
        currentFilePath = path;
    }
//    else
//    {
//        currentFile = QString();
//    }
}


files_finder::runloop()
{
    //this->exec();
    return 0;
}


void files_finder::scanSingleDir(QList <DataContainer*> *list, QDir *dir)
{
    qDebug() << "Scanning directory: "<< *dir;
    QStringList ls = dir->entryList();
    QString path = dir->absolutePath();
    int c = list->size()-1;
    DataContainer * dc;
    foreach (QString f, ls)
    {
        //qDebug() << f;
        if (f!=".." and f!=".")
        {
            dc = new DataContainer(f,path);
            //qDebug() << path + "/" + dc->filename <<"  "<< dc->isLoaded();
            list->append(dc);
            if( currentFilePath == path)
            {
                if (f==currentFile)
                {
                    currentFileIdx = c;
                }
            }
            c++;
         }
    }
    qDebug() << c << " files was found";

}

void files_finder::setUpWatcher()
{
    //setUpWatcher(path);
    connect(fswatch,SIGNAL(directoryChanged(QString)),this,SLOT(directoryChanged(QString)));
    connect(fswatch,SIGNAL(fileChanged(QString)),this,SLOT(fileChanged(QString)));
}

void files_finder::setUpWatcher(QString path)
{
    fswatch = new QFileSystemWatcher(this);
    qDebug()<< "Setting up watcher for "<< path;
    fswatch->addPath(path);
    connect(fswatch,SIGNAL(directoryChanged(QString)),this,SLOT(directoryChanged(QString)));
    connect(fswatch,SIGNAL(fileChanged(QString)),this,SLOT(fileChanged(QString)));
}

void files_finder::setUpWatcher(QStringList ls)
{
    fswatch = new QFileSystemWatcher(this);
    foreach(QString p, ls)
    {
        qDebug()<< "Setting up watcher for path:"<< p;
        fswatch->addPath(p);
    }
//    qDebug()<< "Setting up watcher for list of paths";
//    fswatch->addPaths(ls);
    qDebug()<<fswatch->directories();
    connect(fswatch,SIGNAL(directoryChanged(QString)),this,SLOT(directoryChanged(QString)));
    connect(fswatch,SIGNAL(fileChanged(QString)),this,SLOT(fileChanged(QString)));
}

void files_finder::prepareForScan()
{
    dir = QDir(currentFilePath);
}

void files_finder::loadParams()
{
    loop = loadVariableFromQSettings(*sets,"Loop_over_file_list",(int)LoopFolder);
    ScanSiblings = loadVariableFromQSettings(*sets,"Scan.siblings",false);
}

void files_finder::timerEvent(QTimerEvent *event)
{
    int id = event->timerId();
    qDebug() << "Timer ID:" << id;
    switch(id)
    {
        //case 2: scan_directory(); break;
    }
    //return 0;
}

void files_finder::setLoop(int value)
{
    loop = value;
}



files_finder::scan_directory()
{

    //QStringList ls = dir.entryList();
    list = new QList<DataContainer*>;
    currentFileIdx = -1;
    fswatch = new QFileSystemWatcher(this);

    if (ScanSiblings)
    {
        //qDebug()<< dir;
        if (dir.cdUp())
        {
          //  qDebug()<< dir << dir.absolutePath();
            QStringList ls;
            QDir * cd;
            ls = dir.entryList(QDir::AllDirs);
            QString parpath = dir.absolutePath(),dirname;
            if (ls.size()>2)
                for(int i=2;i< ls.size();i++)
                {
                    dirname = parpath+"/"+ls[i];
                    cd = new QDir(dirname);
                    scanSingleDir(list,cd);
                    fswatch->addPath(dirname);
                }
            setUpWatcher();
        }

    }
    else
    {
        scanSingleDir(list,&dir);
        fswatch->addPath(dir.absolutePath());
        setUpWatcher();
    }
    qDebug()<< "current file is "<< currentFile << " "<< currentFileIdx <<" from "<<list->size();
    emit sigScanComplete();
    return 0;
}

void files_finder::run()
{
    scan_directory();    
}

void files_finder::start()
{

    //worker.start();
}

void files_finder::setFileList(QStringList files)
{
    list = new QList<DataContainer*>;
    QFileInfo fi;
    foreach(QString file,files)
    {
        fi.setFile(file);
        list->append(new DataContainer(fi.fileName(), fi.absolutePath()));
    }
    fi.setFile(files[0]);
    currentFile = fi.fileName();
    currentFileIdx = 0;
    currentFilePath = fi.filePath();
    //emit sigScanComplete();
}

void files_finder::rescan()
{
    qDebug() << "rescan initiated";
    loadParams();
    //setDirectory(currentFilePath);
    prepareForScan();
    scan_directory();
}

QString files_finder::nextFile()
{
    if (list->size()>0)
    {
        if (currentFileIdx==-1) currentFileIdx=0;
        else currentFileIdx++;
        if (currentFileIdx>= list->size())
        {
            switch(loop)
            {
                case noLoop: currentFileIdx=list->size()-1;
                            break;
                case LoopFolder: currentFileIdx=0;
                                    break;
            }
        }
        //currentFile = QString(list[currentFileIdx].filename);
        //DataContainer dc= list[currentFileIdx];
    //    DataContainer * dc;
    //    //dc = list[currentFileIdx];
    //    qDebug()<< typeid(list).name() << endl;
    //    qDebug()<< typeid(list[currentFileIdx]).name() << endl;
    //    qDebug()<< typeid(list->at(currentFileIdx)).name() << endl;
        currentFile = list->at(currentFileIdx)->filename;
        qDebug()<< "nextFile is:" <<currentFile;
//        emit sigNewFileName(currentFile,currentFileIdx);
//        emit sigNewFile(list->at(currentFileIdx));
    }
    return currentFile;
}

QString files_finder::prevFile()
{
    //qDebug()<< currentFile;
    if (list->size()>0)
    {
        if (currentFileIdx==-1) currentFileIdx=list->size()-1;
        else currentFileIdx--;
        if (currentFileIdx< 0)
        {
            switch(loop)
            {
                case noLoop: currentFileIdx=0;
                            break;
                case LoopFolder: currentFileIdx=list->size()-1;
                                    break;
            }
        }
        currentFile = list->at(currentFileIdx)->filename;
        qDebug()<< "nextFile is:" << currentFile;
//        emit sigNewFileName(currentFile,currentFileIdx);
//        emit sigNewFile(list->at(currentFileIdx));
    }
    return currentFile;
}

void files_finder::setScanSiblings(bool)
{
    mode = mode ^= DirectoryScanMode::SiblingsDirectory;
    qDebug()<< "Scan mode changed to "<< mode;
}

void files_finder::directoryChanged(QString d)
{
    qDebug() << "directory changed: "<< d;
    qDebug() << path;
    //setDirectory(currentFilePath);
    prepareForScan();
    scan_directory();
}

void files_finder::fileChanged(QString d)
{
    qDebug() << "file changed: "<< d;
    foreach(DataContainer *dc,*list)
    {
        if (dc->filename==d)
        {
            dc->setLoaded(false);
            qDebug() << "file "<< dc->filename<<" wiil be reloaded";
        }
    }
    //setDirectory(currentFilePath);
    //prepareForScan();
    //scan_directory();
}




//files_finder_thrd::files_finder_thrd(MainWindow *, QString &s)
//{

//}
