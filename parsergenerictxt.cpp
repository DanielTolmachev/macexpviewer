#include "parsergenerictxt.h"
#include <QRegExp>
#include <QDebug>



int LINESTOTRY = 50;
QRegExp re1("[^-+0-9nNaAiIfFeE.,\\s]");
QRegExp re2("\\s");
QStringList sep = QStringList() << "\t" << ","<<" ";

QStringList sl;
QVector<double> parserGenericTxt::tryline(QByteArray *b)
{
    bool ok;
    QVector<double> vd;
    double d;
    int numberscount = 0;
    QList<QByteArray> lb;
    //QString s;
    QString *s = new QString(*b);
//    QString *s1 = new QString(&b);
//    QString *s2 = new QString(b*);
//    QString *s3 = new QString(*b);
    if (re1.indexIn(*s)>-1) //eliminate non-numeriacl string
    {
        //qDebug()<< *s << " "<< re2.indexIn(*s)<< endl;
        return vd;
    }
    sl = s->split(re2,QString::SkipEmptyParts);

    //d = new double(sl.size());
    //lb = b->split(' ');
    foreach(QString ss,sl)
    {
        ok = true;
        d = ss.toDouble(&ok);

        //d = bs.toDouble(&ok);
        //cout << ok <<" "<< d;
        if (ok)
        {
            //qDebug() << "double: "<< d;
            vd.append(d);
            numberscount ++;
        }
        //else qDebug() << "str: "<< ss;
        //cout << bs<<" "<< d << endl;


    }
    return vd;
}

QVector<double> parserGenericTxt::readline(QString *s, int numbers)
{
    //qDebug()<<*s;
    bool ok;
    QVector<double> vd;
    double d;
    int i = 0;
    sl = s->split(re2);
    foreach(QString l, sl)
    {
        try
        {
            //qDebug()<< sl[i];
            d = l.toDouble(&ok);
            if(ok)
            {
                vd.append(d);
                i++;
            }
            if (i>=numbers) break;
        }
        catch(...)
        {
            qDebug() << "Exception while reading line";
        }
    }
    return vd;
}

int parserGenericTxt::readline(DataContainer *dc,QString *s, int cols)
{
    //qDebug()<<*s;
    bool ok;
    //dc->dataV = new QVector<QVector<double> >(numbers);
    //qDebug()<< "size of Qvectors is "<<dc->dataV->at(0).size();
    double d,da[cols];
    int cnt = 0;
    sl = s->split(re2);
    QVector<QVector<double> > dataV(cols);
    foreach(QString l, sl)
    {
        try
        {
            //qDebug()<< sl[i];
            da[cnt] = l.toDouble(&ok);
            if(ok)
            {
                //qDebug()<< "size of " <<i  <<" Qvector is "<<dc->dataV->at(i).size();
                // (*(dc->dataV))[i].append(d);
                //dataV.data()[i].append(d);
                //dataV[i].push_back(d);
                //dataV[i].append(d);
                //qDebug()<< "size of " <<i  <<" Qvector is "<<dc->dataV->at(i).size();
                cnt++;
            }
            if (cnt>=cols) break;
        }
        catch(...)
        {
            qDebug() << "Exception while reading line";
        }

        //qDebug()<< "size of Qvectors is "<<dc->dataV->at(0).size();
    }
    if (cnt==cols)
    {
        for (int k=0;k<cols;k++)
        {
            (*(dc->dataV))[k].append(da[k]);
        }
    }
    return cnt;
}

void parserGenericTxt::setYarray(DataContainer *dc)
{

}

void parserGenericTxt::setUpDc(DataContainer *dc)
{    
    if (dc->cols>1)
    {
        dc->xaxis = 0;
        //TODO xV should become obsolete
        //let's use dataV instead so we can select needed column
        dc->xV = dc->dataV->at(dc->xaxis);
        //dc->dataV->removeAt(dc->xaxis);
    }
    else
    {
        dc->xaxis = -1;
        for (int i=0;i<dc->data_count;i++)
            dc->xV.append(i);
    }
    dc->setLoaded(true);
    qDebug()<< "datacontainer is loaded";
}


/*
 * deprecated
 * used to convert QVector<QVector<double> > to double**
 *
void parserGenericTxt::setUpDc(DataContainer *dc)
{
    int xaxis;
    try
    {
        dc->cols = vvd->at(0).size();
        dc->data_count = vvd->size();
        dc->x = new double[dc->data_count];
        if (dc->cols>1)
        {
            dc->data = new double[(dc->data_count)*(dc->cols-1)];
            xaxis = 0;
        }
        else
        {
            xaxis = -1;
            dc->data = new double[(dc->data_count)];
        }
        int k,j;
        qDebug() << "qvector size is "<< vvd->size();
        QVector<double> v;
        for (unsigned long long i=0;i<dc->data_count;i++)
        {
            //v = vvd[i];
            //qDebug()<< i << " "<< v;
    //        if (i==47)
    //            //qDebug() << "47!";
    //            i=i;
            if (xaxis>-1)        {

                dc->x[i] = vvd->at(i)[xaxis];
                //qDebug()<< "\n x = "<<dc->x[i];
            }
            else dc->x[i] = i;
            for (k=0;k<(dc->cols);k++)
            {
                try
                {
                    //qDebug() << i << k;
        //            for (int j = 0; j < v.size(); ++j)
        //            {
        //                qDebug() << "vector.at("<<j<<") " << v.at(j);
        //            }


        //            qDebug()<<i<<k;
        //            qDebug() << vvd[i][k];
                    //if (k>=vvd[i].size()) ;
                    //else{
                    //if (k==xaxis) dc->x[i] = vvd[i][k];
                    //else
                    //{
                        if ( (k>xaxis) && (xaxis>-1)) j= k-1;
                        else j = k;
                        dc->data[i+j*dc->data_count] = vvd->at(i)[k];
                        //qDebug()<< "\t"<< vvd[i][k];

                    //}
                }
                catch(...)
                {
                    qDebug() << "something happend";
                }
                //}
            }
        }
    }
    catch(...)
    {
        qDebug()<< "setupDc: exception";
    }
    //vvd.clear();
    dc->xaxis = xaxis;
    dc->setLoaded(true);
    qDebug()<< "datacontainer is loaded";
}
*/


void parserGenericTxt::loadData(QFile *f, DataContainer *dc, int cols, int startpos)
{
    f->seek(startpos);
    int expectation = (int) (f->size()-startpos)/dc->bytesperline*1.1;
    int i,cnt =0,colsread,skippedrows = 0;
    QString s;
    QVector<double> v;
    dc->useQVector = true;
    dc->dataV = new QVector<QVector<double>  >(cols);
    for(i=0;i<dc->dataV->size();i++){
        (*(dc->dataV))[i].reserve(expectation);
        }
    dc->cols = cols;
    while(!f->atEnd())
    {
        s = f->readLine();
        //if (cnt>startpos)
        //{
        colsread = readline(dc,&s,cols);
        //qDebug() << "readline returns "<< colsread;
        if (colsread!=cols) //eliminate non-full rows (e.g. last row)
        {
            skippedrows++;
//            qDebug()<< "removing non-full row:"<< s;
//            for (int i=0;i<colsread;i++)
//            {
//                (*(dc->dataV))[i].pop_back();
//            }
        }
        //}
        //cnt++;
    }
    dc->data_count = dc->dataV->at(0).size();
    cout << "data loaded " << dc->dataV->size()
         << "x" <<dc->data_count
         << "; rows skipped:"<< skippedrows<< endl;
    for(i=0;i<dc->dataV->size();i++){
        (*(dc->dataV))[i].squeeze();
        }
}

void parserGenericTxt::detectColumnNames(DataContainer *dc)
{
    int idx = dc->line_start;
    QString s = dc->header.at(idx).trimmed();
    int sep_count = dc->cols-1;
    QString separ;
    foreach (QString separr, sep) {
        if (s.count(separr)==sep_count){
            separ = separr;
            break;
        }
    }
    qDebug()<< "separator is"<< separ;
    if (!separ.isEmpty() && idx>0){
        idx --;
        s = dc->header.at(idx).trimmed();
        QStringList sl =  s.split(separ,QString::SkipEmptyParts);
    //    while(sl[sl.size()-1].isEmpty())
    //        sl.removeLast();
    //    bool stop = false;
    //    while (!stop){
        if( sl.size()==dc->cols){
            dc->column_names = sl;
            qDebug()<< "column_names"<< sl;
            idx --;
            if (idx>-1){
                s = dc->header.at(idx).trimmed();
                sl =  s.split(separ,QString::SkipEmptyParts);
                qDebug()<< "column_names"<< sl;
        //        while(sl[sl.size()-1].isEmpty())
        //            sl.removeLast();
                if( sl.size()==dc->cols){
                    dc->column_units = dc->column_names;
                    dc->column_names = sl;
                    //exchange names units
        //            if dc->column_names
                }
            }
            qDebug()<< "column_names"<< dc->column_names;
        }
        else{qDebug() <<"column names not detected:" <<sl << sl.size()<<dc->cols;
    //        stop = true;
                }
    }
    else qDebug() << "cannot determine separator";

}


void parserGenericTxt::setXarray(DataContainer *dc)
{

}

parserGenericTxt::parserGenericTxt()
{

}

parserGenericTxt::loadToDataContainer(DataContainer *dc,bool* stop)
{
    stopped = stop;
    QString fullname = dc->path+"/"+dc->filename;
    cout << "parseGenTxt: Opening file "<< fullname;
    //cout<< "load status is" << dc->isLoaded();

    QFile f;
    f.setFileName(fullname);
    if(!f.exists())
    {
        cout << "File not found " <<endl;
    }

    return parseTxtFile(&f,dc);

}

parserGenericTxt::parseTxtFile(QFile *f, DataContainer *dc)
{
    //unsigned long long data_size,data_count,i;
    //double x0,dx;
    unsigned long long file_size = f->size();    
    f->open(QIODevice::ReadOnly);
    if(f->isOpen())
    {

        //QDataStream ds;
        //ds.setDevice(f);
        QByteArray b;        
        QVector<double> vd;
        //vvd = new QVector<QVector<double> >;

        //b = f.readAll();
        //QDataStream ds= QDataStream(b,QIODevice::OpenMode::);
        //ds.setByteOrder(QDataStream::LittleEndian);

        int wordcount, wordcount2,linescount = 0;
        unsigned long long startpos,pos = 0;
        int i=0,cline;
        /*******detecting header/data*******/
        for (i=0;i<LINESTOTRY;i++)
        {            
            pos = f->pos();
            if (f->atEnd()) break;
            b = f->readLine();            
            dc->header.append(QString(b));
            //TODO check for EOF
            double *d;
            vd = tryline(&b);
            wordcount2 = vd.size();
            //if (wordcount2>0) dd.append(d);
            //qDebug() << i << ": "<< wordcount2;
            if (i>0)
            {
                if ((wordcount!=wordcount2) || wordcount2 < 1 )
                {
                    wordcount = wordcount2;
                    linescount = 1;
                    startpos = pos;
                    cline = i;
                    //vvd.clear();
                    //vvd.append(vd);

                }

                else
                {
                    linescount += 1;
                    //vvd.append(vd);

                }
            }
            else
            {
                wordcount = wordcount2;
                linescount = 1;
                startpos = pos;
                cline = i;
                //vvd.clear();
                //vvd.append(vd);

            }
            //i++;
            //qDebug()<< i << " lines read\r";

        }
//        int bytesread = f->pos()-startpos;
        dc->bytesperline = (f->pos()-startpos)/linescount;
        qDebug() << "files has "<< wordcount2 << " columns, starting at "<< startpos;
        if (wordcount2>0) cout << " "<<wordcount2 << " columns found at offset "<< startpos<<endl;
        else
        {
            cout << " does not contain any plottable data" <<endl;            
            return 1;
        }
        dc->line_start = cline;
        /**********Load rest of file**************/
        loadData(f,dc,wordcount2,startpos);
        //cout << "totally" << dc->data_count << " lines was loaded" <<endl;
        if (dc->data_count==0)
        {
           // delete vvd;
            return 1;
        }
        setUpDc(dc);
        detectColumnNames(dc);
        //delete vvd;
        //if (vvd.size()>0) vvd.clear();
        return 0;
    }
    return 1;

}

