#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "datacontainer.h"
#include <qdebug.h>

#include <qstring.h>
#include "mainmenu.h"

QBrush plot_backround = Qt::white;

unsigned colorcount = 5;
Qt::GlobalColor color_list []= {Qt::cyan,Qt::black,Qt::blue,Qt::red,Qt::green};

class Zoomer: public QwtPlotZoomer
{
public:
    Zoomer( int xAxis, int yAxis, QWidget *canvas ):
        QwtPlotZoomer( xAxis, yAxis, canvas )
    {
        setTrackerMode( QwtPicker::AlwaysOff );
        setRubberBand( QwtPicker::NoRubberBand );

        // RightButton: zoom out by 1
        // Ctrl+RightButton: zoom out to full size

        setMousePattern( QwtEventPattern::MouseSelect2,
            Qt::RightButton, Qt::ControlModifier );
        setMousePattern( QwtEventPattern::MouseSelect3,
            Qt::RightButton );
    }
};

class Picker : public QwtPlotPicker
{
        //Q_OBJECT

public:
    Picker( int xAxis, int yAxis, RubberBand rubberBand, DisplayMode trackerMode, QWidget* canvas ):
        QwtPlotPicker( xAxis, yAxis, rubberBand, trackerMode, canvas )
    {}


protected:
            virtual QwtText trackerTextF (const QPointF & pos) const
        {



            return QwtText(QString::number(pos.x(),'g',4)+
                           QString::number(pos.y(),'g',4));
        }

};

void MainWindow::loadSettings()
{
    setChecked("Scan.siblings",this->ui->actionView_siblings_folder);
    setChecked("Group_files_with_same_name",ui->actionGroup_files_with_same_name);
    setChecked("Scan.subfolders",ui->actionGroup_files_with_same_name);
    setChecked("Loop_over_file_list",ui->actionLoop_over_file_list);
    setChecked("view.Keep_cols_settings",ui->actionKeep_cols_settings);

    QVariant value;


    value = sets->value("view.Keep_cols_settings");
    if (!value.isNull()){
        optionKeepColumDisp = value.toBool();}

    value = sets->value("view.Use_column_names");
    if (!value.isNull()){
        optionUseColumnsNames = value.toBool();}
}

void MainWindow::setting2ui(QString key, void *callback)
{
    QVariant value;
    value = sets->value(key);
    if (!value.isNull())
    {
        //callback(value.value);
    }

}

void MainWindow::setChecked(QString key, QAction *obj)
{
    QVariant value;
    value = sets->value(key);

            if (!value.isNull())
            {
                obj->setChecked(value.toBool());
                qDebug()<< "setting "<<key<<" "<< value.toBool();
            }
}

/*MainWindow::showPlotfromArray(DataContainer *dc)
{
    if (dc->data_count>0)
    {
        ui->plot->setEnabled(true);
    if (plotterStopped)
    {
        qDebug()<< "showPlot: cancel plotting";
        return -3;
    }
    qDebug() << "showPlot" << dc->filename << " "<<dc->data_count<<" point to be plotted";

    //setupPlot();
    clearPlot();
    // grid


    // curve
    QwtPlotCurve *curve;
    int curves;
    if (dc->xaxis>-1) curves = dc->cols-1;
    else curves = dc->cols;
 //   qDebug()<< "ShowPlot: number of curves"<< curves;
    for (unsigned c=0; c<curves;c++)
    {
        if (plotterStopped) return -3;
        curve = new QwtPlotCurve();

        //curve->setRawSamples(dc->x,
          //                   dc->data,dc->data_count);
        qDebug()<< "plotting curve "<< c << "/"<< curves << endl;

        curve->setRawSamples(dc->x,
                             dc->data+dc->data_count*c,dc->data_count);
        curve->setRenderHint( QwtPlotItem::RenderAntialiased );
        curve->setPen( Qt::white );
        curve->attach(ui->plot);
    }
    ui->plot->replot();
    d_zoomer[0]->setZoomBase(false);
    }
}*/

MainWindow::showPlotfromQVector(DataContainer *dc)
{
    if (dc->data_count>0)
    {
        ui->plot->setEnabled(true);
        if (plotterStopped)
        {
            qDebug()<< "showPlot: cancel plotting";
            return -3;
        }
        qDebug() << "showPlot" << dc->filename << " "<<dc->data_count<<" point to be plotted";

        //setupPlot();
        clearPlot();

        // curves
        QwtPlotCurve *curve;
        int curves;
        if (optionKeepColumDisp){}
        if (dc->xaxis>-1) curves = dc->cols-1;
        else curves = dc->cols;
        //   qDebug()<< "ShowPlot: number of curves"<< curves;

//        if (dc->columns_enabled.size()<dc->cols)
//            dc->columns_enabled.fill(true,dc->cols);

        //show header
        showHeaderStats(dc);

        //display curves
        for (unsigned c=0; c<dc->cols;c++)
        {
            if(c!=dc->xaxis)
            {
                if (plotterStopped) return -3;

                curve = new QwtPlotCurve();

                //curve->setRawSamples(dc->x,
                //                   dc->data,dc->data_count);
                qDebug()<< "plotting column "<< c << "/"<<dc->cols << endl;
                if (dc->useQVector)
                {
                    if (dc->xaxis>-1)
                        curve->setSamples(dc->dataV->at(dc->xaxis),
                                          dc->dataV->at(c));
                    else
                        curve->setSamples(*dc->rowNumbers(),
                                          dc->dataV->at(c));
                }
                else
                {
                    //if (c>0){
                        if (dc->xaxis<0)
                            curve->setRawSamples(dc->rowNumbers()->data(),
                                                 dc->data+dc->data_count*c,
                                                 dc->data_count);
                        else
                            curve->setRawSamples(dc->data+dc->data_count*dc->xaxis,
                                                 dc->data+dc->data_count*c,
                                                 dc->data_count);
                    //}
                }
                int cc = c;
                if(c>=colorcount) cc = c-c/colorcount;
                curve->setRenderHint( QwtPlotItem::RenderAntialiased );
                //curve->setPen( Qt::white );
                //Qt::GlobalColor col = Qt::green;
    //            curve->setPen( col );
//                qDebug()<<cc<<color_list[cc];
                curve->setPen( color_list[cc] );
                if (dc->columns_enabled[c]){
                curve->attach(ui->plot);
                }
            }
        }
        ui->plot->setAxisAutoScale( QwtPlot::xBottom );
        ui->plot->setAxisAutoScale( QwtPlot::yLeft );
        ui->plot->replot();
        d_zoomer[0]->setZoomBase(false);        
        return dc->data_count;
    }
    return 0;
}

void MainWindow::setupColumnControl(DataContainer * dc)
{
    int rowcount = dc->cols+1;
    if (dc->columns_enabled.size()<dc->cols)
         dc->columns_enabled.fill(true,dc->cols);
    ui->tableColumns->setRowCount(rowcount);
    QString column_label;
    QTableWidgetItem * it;
    QRadioButton *rb;
    QCheckBox *cb;
    columns_checkboxes.clear();
    columns_radiobuttons.clear();
    for (int r=0; r<rowcount;r++)
    {
        rb = new QRadioButton;

        if (r==dc->xaxis+1) rb->setChecked(true);
        if (r==0)
            column_label = "row number";
        else {
            cb = new QCheckBox;
            cb->setChecked(dc->columns_enabled[r-1]);
            if (dc->column_names.size()>r-1)
                column_label = dc->column_names[r-1];
            else column_label = "column"+QString::number(r-1);
            if (column_label ==QString::null)
                column_label = "column"+QString::number(r-1);
            ui->tableColumns->setCellWidget(r,1,cb);
            columns_checkboxes.append(cb);
            connect(cb,SIGNAL(toggled(bool)),this,SLOT(on_column_enable(bool)));
        }
        it = new QTableWidgetItem(column_label);
        ui->tableColumns->setCellWidget(r,0,rb);
        columns_radiobuttons.append(rb);
        connect(rb,QCheckBox::toggled,this,MainWindow::on_xaxis_change);
        ui->tableColumns->setItem(r,2,it);

        //this->connect(cb,QCheckBox::toggled,this,MainWindow::on_xaxis_change);
        //w->connect(w,SIGNAL(sigAdvancePrev()),FF,SLOT(prevFile()),Qt::QueuedConnection);
        //connect(w,SIGNAL(filesSelected(QStringList)),this,SLOT(openFiles(QStringList)));
    }


}

void MainWindow::setTitleConst(const QString &value)
{
    titleConst = value;
}


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    sets = new QSettings(COMPANYNAME,PROGRAMNAME);        
    ui->setupUi(this);
//    ui->plot->setAutoFillBackground( true );
//    ui->plot->setPalette( Qt::white );
//    ui->plot->setCanvasBackground( Qt::white );
    ui->statusBar->addPermanentWidget(&SBleft);
    setUpActions();
    if (!sets->value("Window.geometry").isNull())
    {
        QRect geom = sets->value("Window.geometry").value<QRect>();
        if(geom.x() <20) geom.setX(20);
        if(geom.y() <20) geom.setY(20);
        setGeometry(geom);
    }
    loadSettings();
    ui->dock_convert->hide();
    tabifyDockWidget(ui->dockStats,ui->dockHeader);
    ui->dockStats->hide();



    ui->plot->setMouseTracking(true);
    ui->plot->installEventFilter(this);

    d_zoomer[0] = new Zoomer( QwtPlot::xBottom, QwtPlot::yLeft,
        ui->plot->canvas() );
    d_zoomer[0]->setRubberBand( QwtPicker::RectRubberBand );
    d_zoomer[0]->setRubberBandPen( QColor( Qt::green ) );
    d_zoomer[0]->setTrackerMode( QwtPicker::ActiveOnly );
    //d_zoomer[0]->setTrackerPen( QColor( Qt::white ) );
    d_zoomer[0]->setTrackerPen( QColor( Qt::black ) );

    d_zoomer[1] = new Zoomer( QwtPlot::xTop, QwtPlot::yRight,
         ui->plot->canvas() );

    d_panner = new QwtPlotPanner( ui->plot->canvas() );
    d_panner->setMouseButton( Qt::MidButton );
    d_panner->setEnabled(false);

    d_picker = new Picker// QwtPlotPicker
            ( QwtPlot::xBottom, QwtPlot::yLeft,
        QwtPlotPicker::CrossRubberBand, QwtPicker::AlwaysOn,
        ui->plot->canvas() );
    d_picker->setKeyPattern(QwtEventPattern::KeyLeft, 0,Qt::NoModifier);
    d_picker->setKeyPattern(QwtEventPattern::KeyRight, 0,Qt::NoModifier);
    //d_picker->setTrackerMode();
    d_picker->setStateMachine( new QwtPickerDragPointMachine() );
    d_picker->setRubberBandPen( QColor( Qt::green ) );
    d_picker->setRubberBand( QwtPicker::CrossRubberBand );
    //d_picker->setTrackerPen( QColor( Qt::white ) );
    d_picker->setTrackerPen( QColor( Qt::black ) );
    //d_picker->trackerTextF("")
    //*/
    //d_picker->setEnabled(false);

    //grid
    QwtPlotGrid *grid = new QwtPlotGrid;
    grid->enableXMin( true );
//    grid->setMajorPen( Qt::white, 0, Qt::DotLine );
//    grid->setMinorPen( Qt::gray, 0 , Qt::DotLine );
    grid->setMajorPen( Qt::gray, 0, Qt::DotLine );
    grid->setMinorPen( Qt::lightGray, 0 , Qt::DotLine );
    grid->attach( ui->plot );

    //dialogs
    dlg_open.setFileMode(QFileDialog::ExistingFiles);
    printer = new QPrinter(QPrinter::HighResolution);
    dlg_print = new QPrintDialog(printer,this);
    //connections
    connect(this, SIGNAL(sigRetryPlot(DataContainer*)),this,SLOT(loadPlot(DataContainer*)),Qt::QueuedConnection);
    connect(&dlg_open,SIGNAL(filesSelected(QStringList)),this,SIGNAL(filesSelected(QStringList)),Qt::DirectConnection);
    connect(dlg_print,SIGNAL(accepted()),this,SLOT(print()));
    connect(d_picker,SIGNAL(moved(QPoint&)),this,SLOT(d_picker_moved(QPoint&)));
    //ui->plot->canvas()->

}

MainWindow::~MainWindow()
{
    delete ui;
}

int MainWindow::showPlot(DataContainer * dc)
{
    currentDC = dc;
    xaxis = dc->xaxis;
//    if (dc->filename.length())
//    {
//        QString title;
//        title = dc->filename;
//        ui->plot->setTitle(title);
//        setWindowTitle(dc->filename+"  MacExpViewer");
//    }
    setupColumnControl(dc);

    if(!dc->mutex.tryLock())
    {
        qDebug()<<"showPlot:waiting for DataContainer lock"   ;
        emit sigRetryPlot(dc);
    }
//        and (!plotterStopped))
//    {
//
//    }

    if (dc->isLoaded())
    {
        //if (dc->useQVector)
            showPlotfromQVector(dc);
        //else
          //  showPlotfromArray(dc);
    }
    dc->mutex.unlock();
    return 0;
}

void MainWindow::clearPlot()
{
    ui->plot->detachItems(QwtPlotItem::Rtti_PlotCurve,true);
    ui->plot->replot();
}

void MainWindow::disablePlot()
{
    clearPlot();
    ui->plot->setEnabled(false);   
}

void MainWindow::setupPlot()
{
    // plot
    //ui->plot->setCanvasBackground( QColor( "MidnightBlue" ) );
    //ui->plot->setCanvasBackground( plot_backround );
}

void MainWindow::stopPlotter()
{
    plotterStopped = true;
}
void MainWindow::setCurrentFile(const QString &value)
{
    currentFile = value;
    setWindowTitle(value+" - " +titleConst);
}

void MainWindow::setCurrentFile(const QString &name, int &idx)
{
    setCurrentFile(name);
    currentFileIndex = idx;
    SBleft.setText(QString().sprintf("%d/%d",currentFileIndex+1,fileList.size()));
    clearPlot();
}

void MainWindow::setCurrentFile(int idx)
{
    qDebug()<< "set current file"<< idx<<endl;
    currentFileIndex = idx;
    QString msg;
    msg.sprintf("%d/%d",currentFileIndex+1,fileList.size());
    SBleft.setText(msg);    
}

void MainWindow::setCurrentFile(int idx, QStringList list)
{
    fileList = list;

    qDebug()<< "set current file"<< idx<<endl;
    setCurrentFile(idx);

}

void MainWindow::on_MainWindow_sigAdvanceNext()
{
    currentFileIndex++;
    if (currentFileIndex>=fileList.size())
    {
        if (ui->actionLoop_over_file_list->isChecked()) currentFileIndex = 0;
        else currentFileIndex = fileList.size()-1;
    }
    else if (currentFileIndex<0) currentFileIndex = 0;
    setCurrentFile(fileList[currentFileIndex],currentFileIndex);
    emit sigNewFileSet(fileList[currentFileIndex],currentFileIndex);
    ui->plot->setEnabled(false);
    disableInfoFields();
}

void MainWindow::on_MainWindow_sigAdvancePrev()
{
    currentFileIndex--;
    if (currentFileIndex< 0)
    {
        if (ui->actionLoop_over_file_list->isChecked()) currentFileIndex = fileList.size()-1;
        else currentFileIndex = 0;
    }
    else if (currentFileIndex<0) currentFileIndex = fileList.size()-1;
    setCurrentFile(fileList[currentFileIndex],currentFileIndex);
    emit sigNewFileSet(fileList[currentFileIndex],currentFileIndex);
    //clearPlot();
    ui->plot->setEnabled(false);
    disableInfoFields();
}

void MainWindow::d_picker_moved(QPoint &F)
{
    qDebug()<< F;
}

void MainWindow::print()
{
    printer->setCreator("MacExpViewer");
    printer->setOrientation( QPrinter::Landscape );
    QwtPlotRenderer renderer;

//        if ( printer->colorMode() == QPrinter::GrayScale )
//        {
            renderer.setDiscardFlag( QwtPlotRenderer::DiscardBackground );
            renderer.setDiscardFlag( QwtPlotRenderer::DiscardCanvasBackground );
            renderer.setDiscardFlag( QwtPlotRenderer::DiscardCanvasFrame );
            renderer.setLayoutFlag( QwtPlotRenderer::FrameWithScales );
//        }

            renderer.renderTo( ui->plot, *printer );
}

void MainWindow::on_column_enable(bool state)
{
//    qDebug()<<"xaxis"<<currentDC->xaxis<<xaxis;
    while(columnEnableStatus.size()<columns_checkboxes.size())
        columnEnableStatus.append(true);
    for(int i=0; i< columns_checkboxes.size();i++)
    {
        columnEnableStatus[i]=columns_checkboxes[i]->isChecked();
//    if (this->sender()== columns_checkboxes[i]){
//        idx = i;
//        break;}
    }
    //qDebug()<< "channel"<< idx<< "enabled"<<state;
//    qDebug()<<"xaxis"<<currentDC->xaxis;
    reloadPlot();


}

void MainWindow::on_xaxis_change(bool state)
{
    if(state){
        int i,idx=-2;
        for(i=0; i< columns_radiobuttons.size();i++)
        {
        if (this->sender()== columns_radiobuttons[i]){
            idx = i-1;//because 0 corresponds to row number
            break;}
        }
        if (idx>-2)
            xaxis = idx;
        //qDebug()<< "channel"<< idx<< "set as x"<<state;
        reloadPlot();
    }
}


int MainWindow::loadPlot(DataContainer *dc)
{
    plotterStopped = false;
    qDebug() << "loadPlot was called";
    return showPlot(dc);
}

int MainWindow::reloadPlot()
{
//    qDebug()<<"reloadPlot: xaxis"<<currentDC->xaxis<<xaxis;
    if(!currentDC->mutex.tryLock())
    {
        qDebug()<<"showPlot:waiting for DataContainer lock"   ;
        emit sigRetryPlot(currentDC);
    }

    if (currentDC->isLoaded())
    {
            //apply columns and Xaxis choise to datacontainer
            currentDC->xaxis = xaxis;
//            qDebug()<< "xaxis" <<currentDC->xaxis<<xaxis;
            while(columnEnableStatus.size()<currentDC->cols)
                columnEnableStatus.append(true);
            for (int i =0;i<currentDC->cols;i++)
                currentDC->columns_enabled[i] = columnEnableStatus[i];
            //clear plot without refreshing
            //ui->plot->detachItems(QwtPlotItem::Rtti_PlotCurve,true);
            //plot data
            showPlotfromQVector(currentDC);
    }
    currentDC->mutex.unlock();
    return 0;
}

void MainWindow::actions2settings(bool toggle)
{
    qDebug()<< "action toggled by "<<sender()->objectName();
    if(sender()== ui->actionView_siblings_folder)
    {        
        sets->setValue("Scan.siblings",toggle);
        emit rescanRequired();
    }
}

void MainWindow::setCurrenPath(const QString &path)
{
    dlg_open.setDirectory(path);
}


int MainWindow::showPlot()
{
    ui->plot->setTitle("Example");
    return 0;
}

MainWindow::onPlotMouseMove(QMouseEvent * me)
{
    qDebug() << me;
    return 0;
}
void MainWindow::mousePressEvent(QMouseEvent *me)
{
    qDebug() << "mouse button pressed" << me->buttons();
    return;
}

void MainWindow::wheelEvent(QWheelEvent * we)
{
    int delta = we->angleDelta().y();
    if (delta<0)
    {
        emit sigAdvanceNext();
        //emit sigAdvanceNext();//for test purposes
    }
    else if (delta>0) emit sigAdvancePrev();
    //qDebug() << "wheel touched "<< delta;

}

void MainWindow::enableZoomMode( bool on )
{
    d_panner->setEnabled( on );

    d_zoomer[0]->setEnabled( on );
    d_zoomer[0]->zoom( 0 );

    d_zoomer[1]->setEnabled( on );
    d_zoomer[1]->zoom( 0 );

    d_picker->setEnabled( !on );

    showInfo();
}

void MainWindow::showInfo( QString text )
{
    if ( text == QString::null )
    {
        if ( d_picker->rubberBand() )
            text = "Cursor Pos: Press left mouse button in plot region";
        else
            text = "Zoom: Press mouse button and drag";
    }

#ifndef QT_NO_STATUSBAR
    statusBar()->showMessage( text );
#endif
}

void MainWindow::clearInfoFields()
{
    ui->te_header->clear();
    ui->te_stats->clear();
}

void MainWindow::disableInfoFields()
{
    clearInfoFields();
    ui->te_header->setEnabled(false);
    ui->te_stats->setEnabled(false);
}

void MainWindow::showHeaderStats(DataContainer *dc)
{
    if (dc->header.size())
    {
        ui->te_header->setText(dc->header.join(""));
//        foreach (QString hs, dc->header)
//        {
//            ui->te_header->append(hs);
//        }
        ui->te_header->verticalScrollBar()->setValue(0);
        ui->te_header->setEnabled(true);
        ui->dockHeader->show();
        ui->dockHeader->raise();
        if (!dc->stats.size()){
            ui->dockStats->hide();}
    }
    if (dc->stats.size())
    {
        qDebug()<< dc->stats;

        ui->te_stats->setText(dc->stats.join(""));
//        foreach (QString hs, dc->stats)
//        {
//            ui->te_stats->append(hs);
//        }
        ui->te_stats->verticalScrollBar()->setValue(0);
        ui->te_stats->setEnabled(true);
        ui->dockStats->show();
        if (!dc->header.size()){
            ui->dockHeader->hide();}
    }
}

bool MainWindow::eventFilter(QObject *obj, QEvent *evt)
{
    return false;
}

void MainWindow::keyPressEvent(QKeyEvent *evt)
{
    switch(evt->key())
    {
        case Qt::Key_Left : emit sigAdvancePrev(); break;
        case Qt::Key_Right : emit sigAdvanceNext(); break;
        case Qt::Key_Escape : exit(0);
        default: qDebug()<< evt->key();
    }

}

void MainWindow::resizeEvent(QResizeEvent *)
{
    sets->setValue("Window.geometry",geometry());
}

void MainWindow::moveEvent(QMoveEvent *)
{
    sets->setValue("Window.geometry",geometry());
}

void MainWindow::setUpActions()
{
    connect(ui->actionView_siblings_folder,SIGNAL(toggled(bool)),this,SLOT(actions2settings(bool)));
    connect(ui->actionNextFile,SIGNAL(triggered(bool)),this,SIGNAL(sigAdvanceNext()));
    connect(ui->actionPrevFile,SIGNAL(triggered(bool)),this,SIGNAL(sigAdvancePrev()));
    connect(ui->actionOpen,SIGNAL(triggered(bool)),&dlg_open,SLOT(show()));
}

void MainWindow::on_actionGroup_files_with_same_name_triggered(bool checked)
{
    sets->setValue("Group_files_with_same_name",checked);
    qDebug() << "Group_files_with_same_name was set to "<< checked;
}

void MainWindow::on_actionScan_subdirectories_triggered(bool checked)
{
    sets->setValue("Scan.subfolders",checked);
    qDebug() << "Scan.subfolders "<< checked;
}

void MainWindow::on_actionScan_subdirectories_triggered()
{
}

void MainWindow::on_actionView_siblings_folder_triggered(bool checked)
{
    sets->setValue("Scan.siblings",checked);
    qDebug() << "Scan.siblings "<< checked;
}

void MainWindow::on_actionLoop_over_file_list_triggered(bool checked)
{
    sets->setValue("Loop_over_file_list",checked);
}


void MainWindow::on_actionPrint_triggered()
{
    dlg_print->show();
//    QString path = QDir::currentPath()+tr("/radiazione.ps");
//         QPrinter printer(QPrinter::HighResolution);
//         printer.setOutputFileName(path);
//         printer.setCreator("Solar Calc");

//         printer.setOrientation(QPrinter::Portrait);
//         QPrintDialog dialog(&printer);
//        if ( dialog.exec() )
//        {
////        QwtPlotPrintFilter filter;
////            if ( printer.colorMode() == QPrinter::GrayScale )
////            {
////                int options = QwtPlotPrintFilter::PrintAll;
////                options &= ~QwtPlotPrintFilter::PrintBackground;
////                options |= QwtPlotPrintFilter::PrintFrameWithScales;
////                filter.setOptions(options);
////            }

////        print(printer, filter);
//        }
}
