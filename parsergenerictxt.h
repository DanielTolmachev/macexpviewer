#ifndef PARSERGENERICTXT_H
#define PARSERGENERICTXT_H
#include "datacontainer.h"
#include "cout.h"
#include <QFile>

#include <QObject>
#include <QByteArray>
#include <QVector>

class parserGenericTxt
{
    QVector<double> tryline (QByteArray*);
    QVector<double> readline(QString*,int);
    int readline(DataContainer*,QString*,int);
    QVector<QVector<double> > *vvd;    
    void setXarray(DataContainer*);
    void setYarray(DataContainer*);
    void setUpDc(DataContainer*);
    void loadData(QFile *f,DataContainer*dc,int cols, int startrow);
    void detectColumnNames(DataContainer*dc);
    int x_axis;
    bool * stopped;
public:
    parserGenericTxt();
    loadToDataContainer(DataContainer*,bool*);
    parseTxtFile(QFile*,DataContainer*);
};

#endif // PARSERGENERICTXT_H
