#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "datacontainer.h"
#include <QMouseEvent>
#include <qwt_plot_zoomer.h>
#include <qwt_plot_panner.h>
#include <qwt_plot_renderer.h>
#include <qwt_picker_machine.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_grid.h>
#include "cout.h"
#include <QSettings>
#include "program.h"
#include <QLabel>
#include <QFileDialog>
#include <QtPrintSupport/QPrintDialog>
#include <QtPrintSupport/QPrinter>
#include <QScrollBar>
#include <QStringList>
#include <QCheckBox>
#include <QRadioButton>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    void loadSettings();
    void setting2ui(QString value,void*callback);
    void setChecked(QString key,QAction * obj);
    QLabel SBleft;
    int loopmode;
    bool plotterStopped;
    //showPlotfromArray(DataContainer*);
    showPlotfromQVector(DataContainer*);
    void setupColumnControl(DataContainer*);
    QFileDialog dlg_open;
    QPrinter * printer;
    QPrintDialog *dlg_print;
    QVector<QCheckBox*> columns_checkboxes;
    QVector<QRadioButton*> columns_radiobuttons;

signals:
    void sigAdvanceNext();
    void sigAdvancePrev();
    void sigAdvanceNum();
    void sigNewFileSet(QString,int);
    void sigRetryPlot(DataContainer*);
    void filesSelected(QStringList);
    void rescanRequired();
public:
    QString titleConst;
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    int showPlot();
    int showPlot(DataContainer*);
    void setupPlot();
    void stopPlotter();
    int currentFileIndex;
    QString currentFile;
    QStringList fileList;
    DataContainer * currentDC;
    //void setDataList(const QList<DataContainer *> &value);
    int xaxis;
    QVector<bool> columnEnableStatus;
    QVector<QString> columnDisabledList;
    bool optionKeepColumDisp;
    bool optionUseColumnsNames;

    void setTitleConst(const QString &value);

public slots:
    void clearPlot();
    void disablePlot();
    int loadPlot(DataContainer*dc);
    int reloadPlot();

    void testSlot() {cout<<"boo";}
    void actions2settings(bool);
    void setCurrenPath(const QString &path);
    void setCurrentFile(const QString &value);
    void setCurrentFile(const QString &name,int &idx);
    void setCurrentFile(int idx);
    void setCurrentFile(int idx,QStringList list);
    void on_MainWindow_sigAdvanceNext();
    void on_MainWindow_sigAdvancePrev();
    void d_picker_moved(QPoint &);
    void print();
    void on_column_enable(bool state);
    void on_xaxis_change(bool state);




private slots:
    void on_actionGroup_files_with_same_name_triggered(bool checked);

    void on_actionScan_subdirectories_triggered(bool checked);

    void on_actionScan_subdirectories_triggered();

    void on_actionView_siblings_folder_triggered(bool checked);

    void on_actionLoop_over_file_list_triggered(bool checked);

    //void on_dlg_open_filesSelected(const QStringList &fileslist);

    //void on_dlg_open_fileSelected(const QString &file);
    //dlg_open.filesSelected();

    //void on_actionOpen_triggered(bool checked)


    void on_actionPrint_triggered();

private:
    QSettings *sets;
    Ui::MainWindow *ui;
    onPlotMouseMove(QMouseEvent*);
    void mousePressEvent(QMouseEvent*);
    void wheelEvent(QWheelEvent *);
    bool eventFilter(QObject*, QEvent*);
    void keyPressEvent(QKeyEvent*);
    void resizeEvent(QResizeEvent*);
    void moveEvent(QMoveEvent*);
    void setUpActions();
    QwtPlotZoomer *d_zoomer[2];
    QwtPlotPicker *d_picker;
    QwtPlotPanner *d_panner;
    void enableZoomMode( bool );

    void showInfo( QString text = QString::null );
    void clearInfoFields();
    void disableInfoFields();
    void showHeaderStats(DataContainer*dc);
};

#endif // MAINWINDOW_H
