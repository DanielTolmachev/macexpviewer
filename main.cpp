#include "mainwindow.h"
#include <QApplication>
#include <QtCore/qstring.h>
#include "parsermacexp.h"
#include "datacontainer.h"
#include <qwt_plot.h>
#include <qwt_plot_grid.h>

#include <qwt_legend.h>

#include <qwt_plot_curve.h>
#include <qwt_symbol.h>

#include <qwt_plot_magnifier.h>

#include <qwt_plot_panner.h>

#include <qwt_plot_picker.h>
#include <qwt_picker_machine.h>
#include <c++/string>
#include "files_finder.h"
#include <QObject>
#include <QThread>
#include <QMetaObject>
#include <QStringList>
#include "parser.h"
#include "program.h"
#include "controller.h"

//parserMacExp pME = parserMacExp();

DataContainer * dc;




int main(int argc, char *argv[])
{
    //QString testfile = "C:/Users/SFB/Documents/dev/MacExpViewertestfiles/800nm840nmWith798_5";
    //QString testfile2 ="C:\\Users\\SFB\\Documents\\dev\\MacExpViewer\\testfiles\\800nm840nmWith798_5";
    QSettings sets(COMPANYNAME,PROGRAMNAME);
    QString startpath;
    QApplication a(argc, argv);
    a.setApplicationVersion(APP_VERSION);
    //a.setApplicationName(APP_NAME);
    QStringList args = a.arguments();
    files_finder FF;    
    Parser parser;
    qDebug()<< args;


    MainWindow w;
    w.setTitleConst(a.applicationName()+" v"+a.applicationVersion());
    w.setWindowTitle(w.titleConst);    
    Controller control(&w,&FF,&parser);

    //QVariant v = set.value("Window/geometry");
    //if (!v.isNull()):
    w.setupPlot();
    QThread * finderWorker = new QThread();
    QThread * parserWorker = new QThread();
    QThread * controllerWorker = new QThread();
    //files_finder FF(testfile2);    
    //FF.setLoop(loadVariableFromQSettings(sets,"Loop_over_file_list",(int)LoopFolder));
    //FF.ScanSiblings = loadVariableFromQSettings(sets,"Scan.siblings",false);
    FF.moveToThread(finderWorker);
    control.moveToThread(controllerWorker);
    controllerWorker->start();
    finderWorker->start();    
    parser.moveToThread(parserWorker);
    //QObject::connect(&parser,SIGNAL(sigParsingFinished(DataContainer*)),&w,SLOT(loadPlot(DataContainer*)),Qt::QueuedConnection);

    parserWorker->start();
    //dc = pME.load(testfile2);
    if(args.count()==2)
    {
        startpath = args[1];
        FF.setDirectory(startpath);
        dc = new DataContainer(FF.currentFile,FF.path);        
        //qDebug()<< "Opening "<<dc->filename;
        w.setCurrentFile(dc->filename);
        w.setCurrenPath(FF.path);
        //w.currentFile = dc->filename;
        parser.setDataContainer(dc);
        QMetaObject::invokeMethod(&parser,"parse");
        //w.setWindowTitle(w.currentFile + " - "+w.titleConst);
    }
    else if(args.count()<2)
    {
        startpath = ".";
        FF.setDirectory(startpath);
    }
    else {}
    //w.showPlot(dc);    
    w.show();

    QMetaObject::invokeMethod(&FF,"run");
    QObject::connect(&FF,files_finder::sigNewFile,&parser,Parser::parseDC);
    QObject::connect(&w,MainWindow::rescanRequired,&FF,files_finder::rescan);

    //w.connect( SIGNAL(sigAdvancePrev()),&FF,SLOT(prevFile()),Qt::QueuedConnection);
    //w.connect(&w,MainWindow.)

    //QObject::connect(&parser,Parser::sigParsingFinished,&w,MainWindow::loadPlot,Qt::QueuedConnection);

    //QObject::connect(&parser,SIGNAL(sigParsingFinished()),&w,SLOT(testSlot()),Qt::QueuedConnection);
    //QObject::connect(&parser,SIGNAL(testSignal()),&w,SLOT(testSlot()),Qt::QueuedConnection);
    //w.connect(&FF,files_finder::sigNewFileName,&w,MainWindow::showPlot);
//    w.centralWidget()->plot->setAxisTitle(QwtPlot::yLeft, "Y");
//    w.plot->setAxisTitle(QwtPlot::xBottom, "X");
//    w.plot->insertLegend( new QwtLegend() );
    int res = a.exec();
    sets.sync();
    finderWorker->exit(0);
    parserWorker->exit(0);
    //FF.exit(0);
    return res;
}
