#ifndef DATACONTAINER_H
#define DATACONTAINER_H
#include <c++/string>
#include <QString>
#include <QMutex>
#include <QVector>

//using namespace::std;
class DataContainer
{
    int dataloaded;
    QVector<double> _rowNumbers;
public:    
    QMutex mutex;
    DataContainer();
    //DataContainer(wstring);
    DataContainer(QString);
    DataContainer(QString file,QString path);
    double * data,*x;
    QVector<QVector<double> >*dataV;
    QVector<double> xV;
    //std::wstring filename;
    QString filename,path;
    QStringList header,stats;
    QVector<bool> columns_enabled;
    QStringList column_names;
    QStringList column_units;
    unsigned long long data_count;
    unsigned int cols;
    unsigned int line_start;
    int xaxis;
    int format;
    bool isLoaded();
    bool setLoaded(bool);
    bool useQVector;
    double bytesperline;
    QVector<double> * rowNumbers();


};

#endif // DATACONTAINER_H
