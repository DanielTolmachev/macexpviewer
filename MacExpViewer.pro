#-------------------------------------------------
#
# Project created by QtCreator 2015-08-17T15:55:05
#
#-------------------------------------------------

VERSION = 1.10

#DEFINES += APP_NAME="MaxExpViewer"

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MacExpViewer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    parsermacexp.cpp \
    datacontainer.cpp \
    files_finder.cpp \
    formathelper.cpp \
    parser.cpp \
    mainmenu.cpp \
    parsergenerictxt.cpp \
    controller.cpp

HEADERS  += mainwindow.h \
    parsermacexp.h \
    datacontainer.h \
    files_finder.h \
    formathelper.h \
    parser.h \
    cout.h \
    mainmenu.h \
    program.h \
    parsergenerictxt.h \
    controller.h \
    settings.h

FORMS    += mainwindow.ui

CONFIG += qwt



include ( C:/Qwt-6.1.3/features/qwt.prf )
INCLUDEPATH += c:\\qwt-6.1.3\\src
LIBS += -Lc:\\qwt-6.1.3\\src\\qwt-6.0.1c\\lib -lqwt


DEFINES += APP_VERSION=\\\"$$VERSION\\\"

#APP_NAME=\$TARGET\

CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT
#CONFIG += CONSOLE

win32:RC_ICONS += dat_viewer_2013.ico

QT += printsupport

#CONFIG += static
static { #// Everything below takes effect with CONFIG += static

    #CONFIG += static
    #QTPLUGIN += qsqloci qgif

    DEFINES += STATIC #// Equivalent to "#define STATIC" in source code
    message("Static build.")
}
