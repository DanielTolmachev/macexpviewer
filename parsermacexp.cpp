#include "parsermacexp.h"
//#include <c++/fstream>
//#include <c++/string>
//#include "datacontainer.h"
//#include <c++/iostream>
#include <QDataStream>

//#include <QTextStream>
//QTextStream cout(stdout);
#include "cout.h"

#include <qdebug.h>

unsigned int MACEXP_DATASIZE = 1512;
unsigned int MACEXP_X0 = 136;
unsigned int MAXECP_XINC = 144;
unsigned int MAXECP_ELEMENTSIZE = 52;


parserMacExp::parseMacExpFile(QFile *f,DataContainer*dc)
{
    unsigned long long data_size,data_count,i;
    unsigned long long file_size = f->size();
    int elementsize;
    if ((file_size % 8)!=0 )
    {
        cout << dc->filename << " is not macexp file"<<endl;
        return 1;
    }
    f->open(QIODevice::ReadOnly);
    if(f->isOpen())
    {

        QDataStream ds;
        ds.setDevice(f);
        QByteArray b;
        //b = f.readAll();
        //QDataStream ds= QDataStream(b,QIODevice::OpenMode::);
        ds.setByteOrder(QDataStream::LittleEndian);

        f->seek(MAXECP_ELEMENTSIZE);
        ds >> elementsize;
        f->seek(MACEXP_X0);
        ds >> x0 >> dx;
        f->seek(MACEXP_DATASIZE);
        //std::cout<< x0 <<"  "<< dx;

        ds >> data_size;
        if (data_size+ f->pos()!= file_size)
        {
            cout << "\nfile size is " << file_size << "\ndata size is "<< data_size+f->pos()<< endl;
            cout << dc->filename << " is not macexp file"<<endl;
            //cout << "dataloaded = "<<dc->isLoaded()<< endl;

            f->close();
            return 1;
        }
        if (!(elementsize%8))
            dc->cols = elementsize/8+1;
        else
            dc->cols = 2;
        dc->xaxis = 0;
        data_count = data_size/8;
        dc->data_count = data_count/(dc->cols-1);
        dc->data = new double[data_count*dc->cols];
        dc->x = new double[dc->data_count];
        for(i=0;i<dc->data_count;i++)
        {
            for (int k=0; k<dc->cols; k++)
                if (k==0)
                    dc->data[i]=x0+i*dx-dx;
                else
                    ds >> dc->data[i+dc->data_count*k];
            dc->x[i]=x0+i*dx-dx;
            //dc->x[i]=(i*dx+x0;
            //cout << dc->x[i]<< endl;
        }        
        f->close();
        cout << "... done"<< endl;
        cout << "# of columns "<< dc->cols<<endl;
        cout << "# of points "<< dc->data_count<<endl;
        cout << "data_size = " << data_size<< endl;
        //dc->dataloaded = 1;

        setStats(dc);
        dc->setLoaded(true);
        return 0;
    }
    return 1;
}

void parserMacExp::setStats(DataContainer *dc)
{
    QString s("MacExpFile\n\n"
              "Number of points = %1\n"
              "dx = %2");
    s = s.arg(dc->data_count).arg(dx);
    //qDebug()<<"stats are\n"<<s;
    dc->stats.append(s);
//    dc->stats.append("MacExpFile\n");
//    dc->stats.append(QString("Number of points:")+QString(dc->data_count));
//    dc->stats.append(QString("dx = ")+QString(dx));
}

parserMacExp::parserMacExp()
{

}

DataContainer * parserMacExp::load(QString &filename)
{
//    std::ifstream input( filename.c_str(), std::ios::binary );
//    input.seekg(1512);
//    long long data_size;
//    input.read(&data_size,sizeof(data_size));
//    cout << data_size;
    //std::cout << "hello world!";
    cout << "parseMaxExp: Opening file "<< filename;
    DataContainer * dc;
    dc = new DataContainer();
    QFile f;

    f.setFileName(filename);
    if(!f.exists())
    {
        cout << "File not found " <<endl;
    }

    //long long data_size;
    unsigned long long data_size,data_count,i;
    double x0,dx;
    f.open(QIODevice::ReadOnly);
    if(f.isOpen())
    {
        dc->filename = filename;
        unsigned long long file_size = f.size();
        QDataStream ds;
        ds.setDevice(&f);
        QByteArray b;
        //b = f.readAll();
        //QDataStream ds= QDataStream(b,QIODevice::OpenMode::);
        ds.setByteOrder(QDataStream::LittleEndian);

        f.seek(MACEXP_X0);
        ds >> x0 >> dx;
        f.seek(MACEXP_DATASIZE);
        //std::cout<< x0 <<"  "<< dx;

        ds >> data_size;
        if (data_size+ f.pos()!= file_size)
        {
            cout << "\nfile size is " << file_size << "\ndata size is "<< data_size+f.pos()<< endl;
            f.close();
            return dc;
        }
        data_count = data_size/8;
        dc->data = new double[data_count];
        dc->x = new double[data_count];
        for(i=0;i<data_count;i++)
        {
            ds >> dc->data[i];
            dc->x[i]=x0+i*dx-dx;
            //dc->x[i]=(i*dx+x0;
            //cout << dc->x[i]<< endl;
        }
        dc->data_count = data_count;
        f.close();
        cout << "... done"<< endl;
        cout << "data_size = " << data_size<< endl;
    }

    return dc;

}

parserMacExp::loadToDataContainer(DataContainer *dc,bool* stop)
{
    stopped = stop;
    QString fullname = dc->path+"/"+dc->filename;
    cout << "parseMaxExp: Opening file "<< fullname;
    //cout<< "load status is" << dc->isLoaded();

    QFile f;
    f.setFileName(fullname);
    if(!f.exists())
    {
        cout << "File not found " <<endl;
    }

    return parseMacExpFile(&f,dc);



}
