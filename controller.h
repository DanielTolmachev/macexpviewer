#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include "mainwindow.h"
#include "files_finder.h"
#include "parser.h"
#include "datacontainer.h"
#include <QFileDialog>

class Controller:public QObject
{
    Q_OBJECT
    MainWindow *w;
    files_finder *FF;
    Parser * parser;
    QString currentFileName;
    int currentFileIdx;
    int newFile;
    QFileDialog dlg;
public:
    Controller(MainWindow*,files_finder*,Parser*);
    DataContainer * dc;
signals:
    void readytoplot();
    void sigFolderScanFinished(int,QStringList);
    void sigStartParser(DataContainer*);
    void sigParsingFinished(DataContainer*);
    void sigNewFileSet();

public slots:
    void onAdvance(int);
    void onParsingFinished(DataContainer*);
    void onFolderScanFinished();
    void on_w_sigNewFileSet(QString,int);
    void on_sigNewFileSet();
    void openFiles(QStringList);
};



#endif // CONTROLLER_H
