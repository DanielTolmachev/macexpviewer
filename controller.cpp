#include "controller.h"



Controller::Controller(MainWindow *mainwnd, files_finder *filesfndr, Parser *pparser)
{
    w = mainwnd;
    FF = filesfndr;
    parser = pparser;
    newFile = 0;
    connect(FF,files_finder::sigScanComplete,this,Controller::onFolderScanFinished);
    connect(this,SIGNAL(sigFolderScanFinished(int,QStringList)),w,SLOT(setCurrentFile(int,QStringList)));
    connect(w,SIGNAL(sigNewFileSet(QString,int)),this,SLOT(on_w_sigNewFileSet(QString,int)));
    QObject::connect(this,Controller::sigStartParser,parser,Parser::parseDC);
    QObject::connect(parser,SIGNAL(sigParsingFinished(DataContainer*)),this,SLOT(onParsingFinished(DataContainer*)),Qt::QueuedConnection);
    QObject::connect(this,SIGNAL(sigParsingFinished(DataContainer*)),w,SLOT(loadPlot(DataContainer*)),Qt::QueuedConnection);
    connect(this,SIGNAL(sigNewFileSet()),this,SLOT(on_sigNewFileSet()),Qt::QueuedConnection);

    QObject::connect(w,SIGNAL(sigAdvanceNext()),FF,SLOT(nextFile()),Qt::QueuedConnection);
    w->connect(w,SIGNAL(sigAdvancePrev()),FF,SLOT(prevFile()),Qt::QueuedConnection);
    connect(w,SIGNAL(filesSelected(QStringList)),this,SLOT(openFiles(QStringList)));

}

void Controller::onParsingFinished(DataContainer * dc)
{
    w->stopPlotter();
    emit sigParsingFinished(dc);
}

void Controller::onFolderScanFinished()
{
    //w->currentFile = FF->currentFile;
    //w->currentFileIndex = FF->currentFileIdx;
    qDebug()<<"FolderScan is finished";
    QStringList list;
    foreach (DataContainer *dc,*(FF->list))
    {
        list.append(dc->filename);
    }

    emit sigFolderScanFinished(FF->currentFileIdx,list);
}

void Controller::on_w_sigNewFileSet(QString f, int i)
{
    currentFileIdx = i;
    currentFileName = f;
    newFile ++;
    emit sigNewFileSet();
}

void Controller::on_sigNewFileSet()
{
    if (newFile > 1)
    {
        qDebug() << "skipping "<<newFile-1 << " files";
        newFile--;
        return;
    }
    if (currentFileName==FF->list->at(currentFileIdx)->filename)
    {
        qDebug()<<"diverting "<< currentFileName << " to parser";
    }
    else
    {
        qDebug()<< "filenames mismatch"<< FF->list->at(currentFileIdx)->filename;
    }
    if (parser->isRunning())
    {
        parser->stop();
        qDebug()<< "stopping parser";
    }
    else
    {
        qDebug()<< "parser is already stopped";
    }
    emit sigStartParser(FF->list->at(currentFileIdx));

    newFile = 0;
}

void Controller::openFiles(QStringList files)
{
    qDebug()<< files;
    if (files.size()==1)
    {
        FF->setDirectory(files[0]);
        dc = new DataContainer(FF->currentFile,FF->path);
        //w->setCurrentFile(dc->filename);
        QMetaObject::invokeMethod(w,"setCurrentFile",Q_ARG(QString,dc->filename));
        QMetaObject::invokeMethod(w,"disablePlot");
        parser->setDataContainer(dc);
        QMetaObject::invokeMethod(parser,"parse");
        QMetaObject::invokeMethod(FF,"run");
    }
    else if (files.size()>1)
    {
        QMetaObject::invokeMethod(w,"disablePlot");
        QFileInfo fi;
        fi.setFile(files[0]);
        QMetaObject::invokeMethod(w,"setCurrentFile",Q_ARG(QString,fi.fileName()));
        qDebug()<<"creating list of files chosen by user";

        emit sigFolderScanFinished(0,files);

        QMetaObject::invokeMethod(FF,"setFileList",Qt::BlockingQueuedConnection,Q_ARG(QStringList,files));
        //FIX
        dc = new DataContainer(fi.fileName(),fi.filePath());

        qDebug()<< "Opening "<<dc->filename;

        parser->setDataContainer(dc);
        QMetaObject::invokeMethod(parser,"parse");

    }
    else
    {
        qDebug() << "controller.openFiles: wrong parameter"<< files;
    }
}

void Controller::onAdvance(int)
{

}
